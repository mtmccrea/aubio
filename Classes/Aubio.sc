// A SuperCollider Wrapper for Aubio
// brew install aubio
// make sure aubio is installed in /usr/local/bin

Aubio {
	var inputPath, outputPath;
	var aubioCommand, files;
	var outPath;
	var cond;
	classvar aubiocut;

	*initClass {
		aubiocut = "which aubiocut".unixCmdGetStdOut.replace($\n);
	}

	*new { |inputPath, outputPath|
		^super.newCopyArgs(inputPath, outputPath).init
	}

	init {
		files = PathName(inputPath);
		files.isFolder.if({
			files = files.deepFiles
		});
		outputPath.mkdir;
		cond = Condition.new
	}

	cut { |bufSize, hopSize, threshold, onsetMethod, loadCondition, verbose = false|

		{
			// see https://aubio.org/manual/latest/cli.html#aubioonset for help
			files.do{arg pathname;
				outPath = outputPath ++ pathname.fileNameWithoutExtension ++ "/";
				outPath.mkdir;
				aubioCommand = aubiocut + pathname.fullPath.escapeChar($ ) + "-c" + "-o" + outPath.escapeChar($ );
				// add options if needed
				bufSize.notNil.if({
					aubioCommand = aubioCommand + "-B" + bufSize
				});
				hopSize.notNil.if({
					aubioCommand = aubioCommand + "-H" + hopSize
				});
				threshold.notNil.if({
					aubioCommand = aubioCommand + "-t" + threshold
				});
				onsetMethod.notNil.if({
					aubioCommand = aubioCommand + "-O" + onsetMethod
				});
				verbose.if({ aubioCommand.postln });
				aubioCommand.unixCmd({ cond.test_(true).signal });
				cond.wait;
				cond.test_(false)
			};
			"Finished Aubio Cut!!".postln;
			loadCondition !? { loadCondition.test_(true).signal }
		}.forkIfNeeded
	}

	cleanSounds { |low = 0.5, high = 2.0, loadCondition|
		var action = { cond.test_(true).signal };

		{

			PathName(outputPath).deepFiles.do{ |pathname|
				var dur;
				SoundFile.use(pathname.fullPath, { |soundFile|
					dur = soundFile.duration;
				});
				high.notNil.if({
					(dur >= high).if({
						"Removed sound".postln;
						("rm" + pathname.fullPath.escapeChar($ )).unixCmd(action);
						cond.wait;
						cond.test_(false)
					})
				});
				low.notNil.if({
					(dur <= low).if({
						"Removed sound".postln;
						("rm" + pathname.fullPath.escapeChar($ )).unixCmd(action);
						cond.wait;
						cond.test_(false)
					})
				});
			};

			PathName(outputPath).folders.do{ |pathname|
				pathname.entries.isEmpty.if({
					("rmdir" + pathname.fullPath).unixCmd(action);
					cond.wait;
					cond.test_(false)
				})
			};
			"Finished Cleaning Sound Directory!!".postln;
			loadCondition !? { loadCondition.test_(true).signal }

		}.forkIfNeeded
	}

}